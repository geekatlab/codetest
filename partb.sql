SELECT u.id, 
       u.name, 
       Ifnull(r.name, 'buyer') AS `name`, 
       u.email, 
       p.company_name, 
       u.created_at, 
       u.last_login 
FROM   users u 
       INNER JOIN profiles p 
               ON p.user_id = u.id 
       LEFT JOIN model m 
              ON model_id = u.id 
       LEFT JOIN roles r 
              ON r.id = m.role_id 
ORDER  BY u.last_login DESC