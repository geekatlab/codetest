<?php

$info =  "elapsed_time=0.0022132396697998047, type-CNC,radius-1-15,position-1=0.000000000000014,position-1//90,position-2=0%direction-1=-2.0816681711721685e-16";

print_r(getData($info));

function getData($info)
{
    $inputData = explode(',', $info);
    $position = 0;

    foreach ($inputData as $measures) {

        if (!isset($elapsedTime)) {
            $elapsedTime = getElapsedTime($measures);
        }
        
        if (!isset($type)) {
            $type = getTypeString($measures);
        }

        if ($featuresData = getFeatures($measures, $inputData)) {
            $features[] = $featuresData;
        }
        
    }

    $json = $features ?? [];
    $json += $elapsedTime ?? null;
    $json += $type ?? null;
    $json += [
        'feature_count' => count($features)
    ];

    return json_encode($json, JSON_PRETTY_PRINT);
}

function extractData($measures, $string, $seperator = '=')
{
    if (strpos($measures, $string) !== false) {
        list(, $data[$string]) = explode($seperator, $measures);
        return $data ?? null;
    }  
}

function getElapsedTime($measures)
{
    return extractData($measures, 'elapsed_time');
}

function getTypeString($measures)
{
    return extractData($measures, 'type', '-');
}

function getPosition($measures)
{
    if (strpos($measures, '=') !== false) {
        return explode('=', $measures);
    }
}

function getRadius($inputData, $id)
{
    foreach ($inputData as $radius) {
        if (strpos($radius, 'radius-'.$id.'-') !== false) {
            list(,,$radius) = explode('-', $radius);
            return (int) $radius ?? 0;
        }
    }

    return 0;
}

function getDirection($measures)
{
    if (strpos($measures, '%') !== false) {
        list($positionData, $direction) = explode('%', $measures);

        list(,$positionID) = explode('-', getPosition($positionData)[0]);

        if (strpos($direction, '=') !== false && strpos($direction, '-') !== false) {
            list($directionId, $directionValue) = explode('=', $direction);

            list(, $id) = explode('-', $directionId);

            return [
                'id'                => $id ?? 0, 
                'direction_value'   => $directionValue ?? 0,
                'position_id'       => $positionID ?? 0,
            ];
        }
    }
}

function getFeatures($measures, $inputData)
{
       if (strpos($measures, 'position') !== false) {        
            $positionData = getPosition($measures);

            if (strpos($positionData[0], '-') !== false) {
                list(, $positionId) = explode('-',$positionData[0]);
                $features[$positionId]['id'] = $positionId;
            }
                
            //get the direction from the position value
            if ($positionValue = $positionData[1]) {
                if (
                    strpos($positionValue, 'direction') !== false 
                    && $directionData = getDirection($measures)) {
                    $positionValue = $directionData['position_id'];
                    $features[$positionId]['direction'] = $directionData['direction_value'];
                }   
                $features[$positionId]['position'] = (array) $positionValue;
            }

            if (isset($positionId) && $radius = getRadius($inputData, $positionId)) {
                $features[$positionId]['radius'] = $radius;
            }
        }

        return $features ?? [];
}

